import 'package:flutter/material.dart';
import 'package:to_do/Pages/Home.dart';

void main() {
  runApp(MaterialApp(debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}